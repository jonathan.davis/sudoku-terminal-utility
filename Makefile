CC=gcc
CFLAGS=-g -Wall -std=c99
TARGETS=solver

all: $(TARGETS)

$(TARGETS): % : src/%.c
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -rf *.o *.out *~ $(TARGETS) *.sdk

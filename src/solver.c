#include "solver.h"
#include <assert.h>
#include <stdlib.h>

#define TRUE     1
#define FALSE    0

#define SQUARE_SIZE    3
#define GRID_SIZE      SQUARE_SIZE * SQUARE_SIZE

#define ROW_SEPARATOR             "+---+---+---+---+---+---+---+---+---+\n"
#define ROW_SEPARATOR_THICK       "+===+===+===+===+===+===+===+===+===+\n"
#define COLUMN_SEPARATOR          "|"
#define COLUMN_SEPARATOR_THICK    "║"

int main(int argc, char **argv)
{
    printf("Enter the puzzle board one row at a time\n");
    printf("Digits should be separated by spaces and empty cells should be represented by the number 0\n");

    Grid *g = read_stdin();
    printf("\nInitial board:\n");
    print_grid(g);
    printf("\n\n\n");
    if (solve(g)) {
        printf("Solution:\n");
        print_grid(g);
    } else
        printf("Board is unsolvable\n");
    free(g);

    return 0;
}

/* Reads the given grid file and returns a pointer to a new grid */
Grid *read_file(char *filename)
{
    Grid *g = malloc(sizeof(Grid));    // Allocate a new grid struct.
    assert(g != NULL);

    FILE *fp = fopen(filename, "r");
    assert(fp != NULL);

    char *buf = malloc((GRID_SIZE*GRID_SIZE + 1) / 2);    // A grid file is made of packed bytes. We need half as many bytes as we do cells.
    fread(buf, (GRID_SIZE*GRID_SIZE + 1) / 2, sizeof(char), fp);

    int i;
    for (i = 0; i < GRID_SIZE*GRID_SIZE; i++) {
        g->cells[i/GRID_SIZE][i%GRID_SIZE] = (buf[i/2] >> (4 * (i%2)) & 0xF);    // Perform necessary bit-shifting to unpack each byte.
    }

    return g;
}

/* Reads the standard input and returns a pointer to a new grid */
Grid *read_stdin()
{
    Grid *g = malloc(sizeof(Grid));    // Allocate a new grid struct.
    assert(g != NULL);
    
    // Populate the grid from the parsed stdin. This method will be updated to accomodate various grid sizes.
    int i;
    for (i = 0; i < GRID_SIZE; i++) {
        fscanf(stdin, "%d %d %d %d %d %d %d %d %d\n", &g->cells[i][0], &g->cells[i][1], &g->cells[i][2], &g->cells[i][3], &g->cells[i][4],
                &g->cells[i][5], &g->cells[i][6], &g->cells[i][7], &g->cells[i][8]);
    }

    return g;
}

/* Returns true if the row does not contain any duplicate digits (not including 0) */
char row_valid(Grid *g, int row)
{
    int i, j;
    for (i = 0; i < GRID_SIZE; i++) {
        for (j = 0; j < GRID_SIZE; j++) {
            if (g->cells[row][i] == g->cells[row][j] && g->cells[row][i] != 0 && i != j)    // Assert that no two cells have duplicate digits. 
	        return FALSE;
	}
    }
    return TRUE;
}

/* Returns true if the column does not contain any duplicate digits (not including 0) */
char column_valid(Grid *g, int col)
{
    int i, j;
    for (i = 0; i < GRID_SIZE; i++) {
        for (j = 0; j < GRID_SIZE; j++) {
            if (g->cells[i][col] == g->cells[j][col] && g->cells[i][col] != 0 && i != j)    // Assert that no two cells have duplicate digits.
	        return FALSE;
	}
    }
    return TRUE;
}

/* Returns true if the square does not contain any duplicate digits (not including 0) */
char square_valid(Grid *g, int s_row, int s_col)
{
    int i, row_i, col_i, j, row_j, col_j;
    for (i = 0; i < GRID_SIZE; i++) {
        for (j = 0; j < GRID_SIZE; j++) {
            // Convert linear index into square coordinates.
            row_i = i / SQUARE_SIZE + SQUARE_SIZE * s_row;
	    col_i = i % SQUARE_SIZE + SQUARE_SIZE * s_col;
	    row_j = j / SQUARE_SIZE + SQUARE_SIZE * s_row;
	    col_j = j % SQUARE_SIZE + SQUARE_SIZE * s_col;
	    if (g->cells[row_i][col_i] == g->cells[row_j][col_j] && g->cells[row_i][col_i] != 0 && i != j)    // Assert that no two cells have duplicate digits.
                return FALSE;
        }
    }
    return TRUE;
}

/* Returns true if no cells are empty (no cells are 0) */
char is_populated(Grid *g)
{
    int r, c;
    for (r = 0; r < GRID_SIZE; r++) {
        for (c = 0; c < GRID_SIZE; c++) {
            if (!g->cells[r][c]) return FALSE;    // Return false upon first finding of an empty cell.
        }
    }
    return TRUE;
}

/* Return true if the grid is solved. This implies that the grid is both populated and has no duplicate cells */
char is_solved(Grid *g)
{
    if (!is_populated(g)) return FALSE;    // Grid population is a necessary condition for being solved.

    // The grid has GRID_SIZE number of rows, columns, and squares by nature of its geometry.
    // We simply check for any one occurrence of an invalid row, column or square to declare the puzzle unsolved.
    int i;
    for (i = 0; i < GRID_SIZE; i++) {
        if (!row_valid(g, i) || !column_valid(g, i) || !square_valid(g, i/SQUARE_SIZE, i%SQUARE_SIZE))
            return FALSE;
    }
    return TRUE;
}

/* Returns true if changing the cell at the given coordinates to the given digit will not invalidate the cell's row, column, or square */
char move_valid(Grid *g, int row, int col, int digit)
{
    char old = g->cells[row][col];    // Preserve the old value.
    g->cells[row][col] = digit;       // Change to new value for testing.
    char flag = row_valid(g, row) && column_valid(g, col) && square_valid(g, row / SQUARE_SIZE, col / SQUARE_SIZE);    // Determine if all items are valid.
    g->cells[row][col] = old;         // Revert cell to previous value.
    return flag;
}

/* Recursively solves the Grid. Returns true if the grid was solved. Returns false if the grid is unsolvable. The grid is only changed if it was solved. */
char solve(Grid *g)
{
    if (is_solved(g)) return TRUE;        // If the grid is already solved, we do not need to do any further work.
    if (is_populated(g)) return FALSE;    // If the grid is not solved, but is populated, we know that the board cannot be solved.

    int r, c, i;
    for (r = 0; r < GRID_SIZE; r++) {
        for (c = 0; c < GRID_SIZE; c++) {
            // Locate the first empty cell.
            if (!(g->cells[r][c])) {
                for (i = 1; i <= GRID_SIZE; i++) {
                    // Check every digit to find a valid move.
                    if (move_valid(g, r, c, i)) {
                        // If the move is valid, check if it solves the board.
                        g->cells[r][c] = i;
                        if (solve(g)) return TRUE;
                        g->cells[r][c] = 0;    // The move made the board unsolvable if this point is reached. We revert back to 0 try a different move.
                    }
                }
		return FALSE;    // At this point, there was no valid move such that the grid became solvable. This means that the puzzle cannot be solved.
            }
        }
    }
    return FALSE;
}

/* Prints the grid to the screen */
void print_grid(Grid *g) {
    printf(ROW_SEPARATOR_THICK);
    int r, c;
    for (r = 0; r < GRID_SIZE; r++) {
        printf(COLUMN_SEPARATOR_THICK);
        for (c = 0; c < GRID_SIZE; c++) {
            if (g->cells[r][c])
                printf(" %d ", g->cells[r][c]);
	    else
                printf("   ");
	    if ((c+1) % SQUARE_SIZE == 0)
 	        printf(COLUMN_SEPARATOR_THICK);
	    else
                printf(COLUMN_SEPARATOR);
        }
	printf("\n");
	if ((r+1) % SQUARE_SIZE == 0)
            printf(ROW_SEPARATOR_THICK);
        else
            printf(ROW_SEPARATOR);
    }
}

/* Writes the grid to the given file */
char write_grid(Grid *g, char *filename)
{
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) return FALSE;

    char *buf = calloc((SQUARE_SIZE*SQUARE_SIZE + 1) / 2, sizeof(char));          // Each byte can store two digits, so we need half as many bytes as we have cells.
    int i;
    for (i = 0; i < GRID_SIZE*GRID_SIZE; i++) {
        buf[i/2] |= ((char) g->cells[i/GRID_SIZE][i%GRID_SIZE]) << (4 * (i%2));    // Shift every other digit to the left by 4 bits to pack the bytes.
    }
    fwrite(buf, (SQUARE_SIZE*SQUARE_SIZE + 1) / 2, sizeof(char), fp);
    free(buf);

    return TRUE;
}

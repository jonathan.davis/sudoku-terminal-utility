#include <stdio.h>

struct TGrid {
    int cells[9][9];
} typedef Grid;

/* Returns pointer to a grid from file */
Grid *read_file(char *filename);

/* Returns pointer to a grid from standard input */
Grid *read_stdin();

/* Returns true if the row contains no duplicate digits */
char row_valid(Grid *g, int row);

/* Returns true if the column contains no duplicate digits */
char column_valid(Grid *g, int col);

/* Returns true if the square contains no duplicate digits */
char square_valid(Grid *g, int s_row, int s_col);

/* Returns true if the gris is populated */
char is_populated(Grid *g);

/* Returns true if the grid is populated and has no invalid cells */
char is_solved(Grid *g);

/* Returns true if the move at the given cell position will not invalidate the row, column, or square of the cell */
char move_valid(Grid *g, int row, int col, int digit);

/* Returns true if the grid was solved. Returns false otherwise */
char solve(Grid *g);

/* Prints a graphical representation of the grid to the screen */
void print_grid(Grid *g);

/* Writes the grid to the given file. Returns true if successful */
char write_grid(Grid *g, char *filename);
